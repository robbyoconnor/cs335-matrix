/******************************************************************************
 Title: matrix.h
 Author: Robert O'Connor
 Created on: February 7, 2013
 Description: A class that represents an n x m matrix,
 where n is rows and m is the number of columns.
 *******************************************************************************/
#ifndef MATRIX_H_
#define MATRIX_H_

#include <iostream>
using namespace std;

/**
 * A class that represents an n by m matrix and allows for
 * arithmetic operations to be performed, given that dimensions of the matrices
 * are equal.
 */
template<typename T>
class matrix {
	public:

		/**
		 * Creates an empty (0x0) matrix.
		 */
		matrix();

		// construct a matrix whose size is rowsxcols.g
		matrix(const int rows, const int cols);

		// copy ctor
		matrix(const matrix &other);

		// dtor
		~matrix();

		/**
		 * Override assignment operator to act as copy constructor.
		 * Delegates to the copy constructor.
		 */
		matrix &operator=(const matrix<T> &);

		/**
		 * Get the number of Rows
		 * @return the number of rows
		 */
		int numrows() const{
			return rows;
		}

		/**
		 * Get the number of columns
		 * @return the number of columns
		 */
		int numcols() const{
			return cols;
		}

		//operator overloading stuff
		// there be dragons up in here! Watch your step...
		matrix &operator+=(const matrix &);

		template<typename Object>
		friend matrix operator+(const matrix<Object> &m1, const matrix<Object>&m2){
			matrix<int> res(m1.rows,m1.cols);
			if (m1.rows!=m2.rows||m1.cols!=m2.cols)
				throw "Error: dimensions not equal.";
			for(int i=0;i<m1.rows;++i) {
				for(int j=0;j<m1.cols;++j) {
					res.arr[i][j] = m1.arr[i][j] + m2.arr[i][j];
				}
			}
			return res;
		}

		matrix operator*(const matrix &);

		template<class Object>
		friend ostream &operator<<(ostream &out, const matrix<Object>&m){
			out<<"\n[";
			for(int i = 0;i<m.rows;++i) {
				for(int j = 0;j<m.cols;++j) {
					out<<m[i][j]<<" ";
				}
				if (i!=m.rows-1)
					cout<<endl;
			}
			out<<"]"<<std::endl;
			return out;
		}

		template<class Object>
		friend istream &operator>>(istream &in, matrix<Object> &m){
			int rows = 0, cols = 0;
			cout<<"Enter Rows: ";
			in>>rows;
			cout<<"\nEnter Cols: ";
			in>>cols;
			m.rows = rows;
			m.cols = cols;
			makeArrayFor(m);
			cout<<"\nEnter the matrix (one space between the numbers): ";
			for(int i = 0;i<rows;++i) {
				for(int j = 0;j<cols;++j) {
					in>>m.arr[i][j];
				}
			}
			return in;
		}

		// for scalar*matrix
		template<class Object>
		friend matrix<Object> operator*(int scalar, const matrix<Object> &rhs){
			matrix<Object> temp(rhs);
			for(int i = 0;i<temp.numrows();++i) {
				for(int j = 0;j<temp.numcols();++j) {
					temp.arr[i][j] = scalar*rhs.arr[i][j];
				}
			}
			return temp;
		}

		T* operator[](int idx);

		T* operator[](int idx) const;

		template<typename Object>
		friend void makeArrayFor(matrix<Object> &matrix);
	private:
		T **arr;
		int rows;
		int cols;
};

// friend function for allocation.
template<typename T>
void makeArrayFor(matrix<T> &m){
	m.arr = new T*[m.rows];
	for(int i = 0;i<m.rows;i++) {
		m.arr[i] = new T[m.cols];
	}
}
// implementation
template<typename T>
matrix<T>::matrix()
		: rows(0), cols(0){
	makeArrayFor(*this);
	for(int i = 0;i<rows;++i) {
		for(int j = 0;j<cols;++j) {
			arr[i][j] = 0;
		}
	}
}

template<typename T>
matrix<T>::matrix(const int rows, const int cols)
		: rows(rows), cols(cols){
	makeArrayFor(*this);
	;
	for(int i = 0;i<rows;++i) {
		for(int j = 0;j<cols;++j) {
			arr[i][j] = 0;
		}
	}
}

template<typename T>
matrix<T>::matrix(const matrix<T>& rhs){
	rows = rhs.rows;
	cols = rhs.cols;

	makeArrayFor(*this);
	;
	for(int i = 0;i<rows;++i) {
		for(int j = 0;j<cols;++j) {
			arr[i][j] = 0;
		}
	}
}

template<typename T>
matrix<T>::~matrix(){
	for(int i = 0;i<rows;++i) {
		delete[] arr[i];
	}
	delete[] arr;
	arr = NULL;
}

template<typename T>
matrix<T>& matrix<T>::operator=(const matrix<T>& other){

	//Dealloacate space
	for(int i = 0;i<rows;i++) {
		delete[] arr[i];
	}
	delete[] arr;
	arr = NULL;
	//Allocate space for this
	if (this!=&other) {

		rows = other.rows;
		cols = other.cols;

		makeArrayFor(*this);

		for(int i = 0;i<other.rows;++i) {
			for(int j = 0;j<other.cols;++j) {
				arr[i][j] = other.arr[i][j];
			}
		}
	}
	return *this;
}

template<typename T>
matrix<T>& matrix<T>::operator +=(const matrix<T>& rhs){
	if ((rhs.numcols()!=this->numcols())||(rhs.numrows()!=this->numrows())) {
		throw "Dimensions must match.";
	} else {
		for(int i = 0;i<rows;++i) {
			for(int j = 0;j<cols;++j) {
				arr[i][j] += rhs.arr[i][j];
			}
		}
	}

	cout<<*this;
	return *this;
}

template<typename T>
matrix<T> matrix<T>::operator*(const matrix<T>& rhs){
	matrix<T> res(this->rows, rhs.numcols());
	for(int i = 0;i<res.numrows();++i) {
		for(int j = 0;j<res.numcols();++j) {
			for(int k = 0;k<this->rows;++k) {
				res.arr[i][j] += this->arr[i][k]*rhs.arr[k][j];
			}
		}
	};

	return res;
}

template<typename T>
T* matrix<T>::operator[](int idx) const{
	return arr[idx];
}

template<typename T>
T* matrix<T>::operator[](int idx){
	return arr[idx];
}

#endif