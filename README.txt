Assignment 1
Robert  O'Connor (roconnor@hunter.cuny.edu)

--Implemented in a bit of a different way. Input can be done via a redirecting a textfile with the following for example using the sample data 
from the assignment: 
3 <- 
3 <- cols 
1 2 3 6 5 4 9 8 10 <- matrix data 

3 <- rows
2 <- cols
9 1 2 3 4 5 <- matrix data

File is provided to you as inputs.txt.

Enter rows, Enter cols, Enter matrix data. Sorry for the change of spec. It works as it should though!

-- No known bugs

--To compile (and run!) just type

         make all

Thank you for being so patient and sorry it is late!